package com.example.proyectotest.modelo;

public class Saldos {
    long cuenta;
    float saldoGeneral,ingresos,gastos;
    int id;
    public Saldos(long cuenta, float saldoGeneral, float ingresos, float gastos, int id) {
        this.cuenta = cuenta;
        this.saldoGeneral = saldoGeneral;
        this.ingresos = ingresos;
        this.gastos = gastos;
        this.id = id;
    }

    public Saldos() {
    }

    public void setCuenta(long cuenta) {
        this.cuenta = cuenta;
    }

    public void setSaldoGeneral(float saldoGeneral) {
        this.saldoGeneral = saldoGeneral;
    }

    public void setIngresos(float ingresos) {
        this.ingresos = ingresos;
    }

    public void setGastos(float gastos) {
        this.gastos = gastos;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCuenta() {
        return cuenta;
    }

    public float getSaldoGeneral() {
        return saldoGeneral;
    }

    public float getIngresos() {
        return ingresos;
    }

    public float getGastos() {
        return gastos;
    }

    public int getId() {
        return id;
    }



}

package com.example.proyectotest;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.proyectotest.adaptadores.ListAdapter;
import com.example.proyectotest.adaptadores.ListAdapterMovimientos;
import com.example.proyectotest.modelo.Movimientos;
import com.example.proyectotest.modelo.Saldos;
import com.example.proyectotest.modelo.Tarjetas;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Inicial extends Fragment  {
    private static final String urlSaldos="http://bankapp.endcom.mx/api/bankappTest/saldos";
    private static final String urlTarjetas="http://bankapp.endcom.mx/api/bankappTest/tarjetas";
    private static final String urlMovimientos="http://bankapp.endcom.mx/api/bankappTest/movimientos";


    RequestQueue requestQueue;
    TextView agregarTarjeta,saldoGeneral,saldoIngresos,saldoGastos;
    Saldos saldos;

    public List<Movimientos> movimientos;
    List<Tarjetas> tarjetas;
    View view;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public Inicial() {
        // Required empty public constructor
    }


    public static Inicial newInstance(String param1, String param2) {
        Inicial fragment = new Inicial();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_inicial, container, false);
        requestQueue= Volley.newRequestQueue(view.getContext());
        iniciaLosTexview();
        ConsultaSaldos();
        agregarTarjeta=(TextView)view.findViewById(R.id.agregar);
        agregarTarjeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent agregar=new Intent(getContext(),AsociarTarjeta.class);
                startActivity(agregar);
            }
        });
        iniciarMovimientos();

        iniciarTarjetas();
        return view;
    }
    public void iniciaLosTexview(){
        saldoGeneral=(TextView)view.findViewById(R.id.saldoGeneral);
        saldoGastos=(TextView)view.findViewById(R.id.saldoGastos);
        saldoIngresos=(TextView)view.findViewById(R.id.saldoIngresos);

    }
    public void iniciarTarjetas(){
        ConsultaTarjetas();

    }
    public void iniciarMovimientos(){
        ConsultaMovimientos();


    }

    private void ConsultaSaldos() {
        saldos=new Saldos();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                urlSaldos,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("saldos");
                            int size = jsonArray.length();
                            saldos=new Saldos();
                            for (int i = 0; i < size;i++){
                                JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                saldos.setId(jsonObject.getInt("id"));
                                saldos.setCuenta(jsonObject.getLong("cuenta"));
                                saldos.setGastos((float)jsonObject.getDouble("gastos"));
                                saldos.setIngresos((float)jsonObject.getDouble("ingresos"));
                                saldos.setSaldoGeneral((float)jsonObject.getDouble("saldoGeneral"));
                                saldoGeneral.setText(saldos.getSaldoGeneral()+"");
                                saldoGastos.setText(saldos.getGastos()+"");
                                saldoIngresos.setText(saldos.getIngresos()+"");

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        requestQueue.add(jsonObjectRequest);
    }

    private void ConsultaMovimientos() {
        movimientos=new ArrayList<>();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                urlMovimientos,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("movimientos");
                            int size = jsonArray.length();
                            Movimientos mov=new Movimientos();
                            for (int i = 0; i < size;i++){
                                JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());

                                //Toast.makeText(view.getContext(), mov.getTipo(), Toast.LENGTH_SHORT).show();
                                movimientos.add(new Movimientos(jsonObject.getString("fecha"),jsonObject.getString("descripcion"),jsonObject.getString("tipo"),jsonObject.getInt("id"), ((float) jsonObject.getDouble("monto"))));
                            }

                            ListAdapterMovimientos listAdapterMovimientos=new ListAdapterMovimientos(movimientos,view.getContext());
                            RecyclerView recyclerView=view.findViewById(R.id.listReciclerViewMovimientos);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                            recyclerView.setAdapter(listAdapterMovimientos);
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        requestQueue.add(jsonObjectRequest);

    }
    private void ConsultaTarjetas() {
        tarjetas=new ArrayList<>();
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                urlTarjetas,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("tarjetas");
                            int size = jsonArray.length();
                            Movimientos mov=new Movimientos();
                            for (int i = 0; i < size;i++){
                                JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                tarjetas.add(new Tarjetas(jsonObject.getString("tarjeta"),(float)jsonObject.getDouble("saldo"),jsonObject.getInt("id"),jsonObject.getString("nombre"),  jsonObject.getString("estado"),jsonObject.getString("tipo")));
                            }

                            ListAdapter listAdapter=new ListAdapter(tarjetas,view.getContext());
                            RecyclerView recyclerView=view.findViewById(R.id.listReciclerView);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                            recyclerView.setAdapter(listAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        requestQueue.add(jsonObjectRequest);
    }


}
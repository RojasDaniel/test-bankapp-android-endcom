package com.example.proyectotest.modelo;

public class Tarjetas {
    String tarjeta;
    float saldo;
    int id;
    String nombre,estado,tipo;
    public Tarjetas(String tarjeta, float saldo, int id, String nombre, String estado, String tipo) {
        this.tarjeta = tarjeta;
        this.saldo = saldo;
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.tipo = tipo;
    }

    public Tarjetas() {
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public float getSaldo() {
        return saldo;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getEstado() {
        return estado;
    }

    public String getTipo() {
        return tipo;
    }



}

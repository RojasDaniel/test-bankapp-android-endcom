package com.example.proyectotest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class AsociarTarjeta extends AppCompatActivity implements View.OnClickListener {
    Button agregar,cancelar;
    TextView numTarjeta,numCuenta,numIssure,nomTarjeta,marca,estadoTarjeta,saldoTarjeta,tipoCuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asociar_tarjeta);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logobank);
        agregar=(Button)findViewById(R.id.agregar);
        agregar.setOnClickListener(this);

        numTarjeta=(TextView)findViewById(R.id.numTarjeta);
        numCuenta=(TextView)findViewById(R.id.numCuenta);
        numIssure=(TextView)findViewById(R.id.numIssure);
        nomTarjeta=(TextView)findViewById(R.id.nomTarjeta);
        marca=(TextView)findViewById(R.id.marca);
        estadoTarjeta=(TextView)findViewById(R.id.estadoTarjeta);
        saldoTarjeta=(TextView)findViewById(R.id.saldoTarjeta);
        tipoCuenta=(TextView)findViewById(R.id.tipoCuenta);;

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.agregar){
            JSONObject obj = new JSONObject();
            try {
                obj.put("tarjeta", numTarjeta.getText().toString());
                obj.put("cuenta", numCuenta.getText().toString());
                obj.put("issure", numIssure.getText().toString());
                obj.put("nombreTarjeta", nomTarjeta.getText().toString());
                obj.put("marca", marca.getText().toString());
                obj.put("estado", estadoTarjeta.getText().toString());
                obj.put("saldo", saldoTarjeta.getText().toString());
                obj.put("tipo", tipoCuenta.getText().toString());
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Datos Ingresados");
            builder.setMessage(obj.toString());
            builder.setPositiveButton("Aceptar", null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
package com.example.proyectotest.modelo;

public class Cuenta {
    long cuenta;
    int id;
    String nombre,ultimaSesion;
    //constructor
    public Cuenta(Long cuenta, int id, String nombre, String ultimaSesion) {
        this.cuenta = cuenta;
        this.id = id;
        this.nombre = nombre;
        this.ultimaSesion = ultimaSesion;
    }

    public Cuenta() {
    }

    //setter
    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }

    //guetter

    public Long getCuenta() {
        return cuenta;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }




}

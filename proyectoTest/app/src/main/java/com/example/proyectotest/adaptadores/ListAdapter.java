package com.example.proyectotest.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectotest.R;
import com.example.proyectotest.modelo.Tarjetas;

import java.util.List;

public class ListAdapter  extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<Tarjetas> mData;
    private LayoutInflater mInflater;
    private Context context;

    public ListAdapter(List<Tarjetas> itemList,Context context){
        this.mInflater=LayoutInflater.from(context);
        this.context=context;
        this.mData=itemList;
    }
    @Override
    public int getItemCount(){ return mData.size();}
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view=mInflater.inflate(R.layout.listelement,null);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int position){
        holder.bindData(mData.get(position));
    }
    public void setItems(List<Tarjetas> items){mData=items;}

    public  class ViewHolder extends RecyclerView.ViewHolder{
        TextView saldoCuenta,tipo,nombre,cuenta,estado;
        ImageView iconoActiva;
        ViewHolder(View itemView){
            super(itemView);
            iconoActiva=itemView.findViewById(R.id.iconoActiva);
            saldoCuenta=itemView.findViewById(R.id.saldoCuenta);
            tipo=itemView.findViewById(R.id.tipo);
            nombre=itemView.findViewById(R.id.nombre);
            cuenta=itemView.findViewById(R.id.cuenta);
            estado=itemView.findViewById(R.id.estado);
        }
        void bindData(final Tarjetas item){
            if(item.getEstado().equals("activa")){
                iconoActiva.setImageResource(R.drawable.tarjetaactiva);
            }else{
                iconoActiva.setImageResource(R.drawable.tarjetainactiva);
            }
            saldoCuenta.setText("$"+item.getSaldo()+"");
            tipo.setText(item.getTipo()+"");
            nombre.setText(item.getNombre()+"");
            cuenta.setText(item.getTarjeta()+"");
            estado.setText(item.getEstado()+"");

        }
    }
}

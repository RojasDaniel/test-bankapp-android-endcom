package com.example.proyectotest.modelo;

public class Movimientos {
    int id;
    float monto;
    String fecha,descripcion,tipo;
    public Movimientos(String fecha, String descripcion, String tipo, int id, float monto) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.id = id;
        this.monto = monto;
    }

    public Movimientos() {
    }

    public String getFecha() {
        return fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public int getId() {
        return id;
    }

    public float getMonto() {
        return monto;
    }


    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }



}

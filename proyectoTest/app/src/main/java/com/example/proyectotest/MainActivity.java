package com.example.proyectotest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button fragmentInicial,fragmentEnviar;
    FragmentTransaction transaction;
    Fragment fragmentInicio,fragmentEnviarDinero;
    TextView nombreCuenta,ultimoInicio;
    private static final String urlCuenta="http://bankapp.endcom.mx/api/bankappTest/cuenta";
    RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logobank);
        requestQueue= Volley.newRequestQueue(this);


        nombreCuenta=(TextView)findViewById(R.id.nombre);

        ultimoInicio=(TextView)findViewById(R.id.ultimoInicio);
        fragmentInicial=(Button)findViewById(R.id.miscuentas);
        fragmentEnviar=(Button)findViewById(R.id.enviardinero);
        fragmentInicial.setOnClickListener(this);
        fragmentEnviar.setOnClickListener(this);
        fragmentInicial.setTextColor(Color.parseColor("#0493bb"));
        fragmentEnviar.setTextColor(Color.parseColor("#acaaac"));

        //instanciamos los fragment #acaaac
        fragmentInicio=new Inicial();
        fragmentEnviarDinero=new EviarDinero();
        ConsultaCuenta();
        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragment,fragmentInicio).commit();

    }

    @Override
    public void onClick(View v) {
        transaction=getSupportFragmentManager().beginTransaction();
        int id=v.getId();
        if(id==R.id.miscuentas){

            fragmentInicial.setTextColor(Color.parseColor("#0493bb"));
            fragmentEnviar.setTextColor(Color.parseColor("#acaaac"));
            transaction.replace(R.id.contenedorFragment,fragmentInicio).commit();
            transaction.addToBackStack(null);
        }
        if(id==R.id.enviardinero){

            fragmentInicial.setTextColor(Color.parseColor("#acaaac"));
            fragmentEnviar.setTextColor(Color.parseColor("#0493bb"));
            transaction.replace(R.id.contenedorFragment,fragmentEnviarDinero).commit();
            transaction.addToBackStack(null);
        }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    private void ConsultaCuenta() {
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(
                Request.Method.GET,
                urlCuenta,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("cuenta");
                            int size = jsonArray.length();
                            for (int i = 0; i < size;i++){
                                JSONObject jsonObject=new JSONObject(jsonArray.get(i).toString());
                                String nombreRecibido =jsonObject.getString("nombre");
                                String ultimaSesion=jsonObject.getString("ultimaSesion");
                                String[] fecha = ultimaSesion.split(" ");
                                nombreCuenta.setText(nombreRecibido+"");
                                ultimoInicio.setText("Último inicio "+fecha[0]);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }
        );
        requestQueue.add(jsonObjectRequest);
    }
}

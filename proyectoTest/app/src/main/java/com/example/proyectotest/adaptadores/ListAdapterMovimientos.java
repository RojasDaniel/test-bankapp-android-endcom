package com.example.proyectotest.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.proyectotest.R;
import com.example.proyectotest.modelo.Movimientos;

import java.util.List;

public class ListAdapterMovimientos extends RecyclerView.Adapter<ListAdapterMovimientos.ViewHolder> {
        private List<Movimientos> mData;
        private LayoutInflater mInflater;
        private Context context;

        public ListAdapterMovimientos(List<Movimientos> itemList,Context context){
            this.mInflater=LayoutInflater.from(context);
            this.context=context;
            this.mData=itemList;
        }
        @Override
        public int getItemCount(){ return mData.size();}
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
            View view=mInflater.inflate(R.layout.listamovimientos,null);
            return new ViewHolder(view);
        }
        @Override
        public void onBindViewHolder(final ViewHolder holder,final int position){
            holder.bindData(mData.get(position));
        }
        public void setItems(List<Movimientos> items){mData=items;}

        public  class ViewHolder extends RecyclerView.ViewHolder{
            TextView fecha,monto,descripcion;
            ViewHolder(View itemView){
                super(itemView);
                fecha=itemView.findViewById(R.id.fechaMovimiento);
                monto=itemView.findViewById(R.id.montoMovimiento);
                descripcion=itemView.findViewById(R.id.tipoMovimiento);
            }
            void bindData(final Movimientos item){
                fecha.setText(item.getFecha()+"");
                descripcion.setText(item.getTipo()+"");
                monto.setText("$"+item.getMonto()+"");

            }
        }
    }
